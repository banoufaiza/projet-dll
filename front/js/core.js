function core() {
	// set all the used variables
	let audio = document.getElementById('audio');

	let rendering = false;

	let width_img = 700
	let height_img = 600;

	let imageCompare = null;

	let oldImage = null;

	let topLeft = [Infinity, Infinity];
	let bottomRight = [0, 0];

	// start the animation from the video
	let raf = (function () {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame ||
			function (anim) {
				setTimeout(anim, 1000 / 60);
			};
	})();

	/*
     * Initializes the function image_compare
     */
	function initialize() {
		imageCompare = image_compare();

		rendering = true;

		main();
	}

	/*
     * Compares to images and updates the position
     * of the motion div.
     */

	// let count=0;
	// let render = setInterval(function(){
	// 		if(count == 5) {
	// 			clearInterval(render);
	// 			setTimeout(render, 5000)
	// 		}
	// 		count++;
	// 		console.log('hey')
	// }, 1000);


	function render() {
		oldImage = new Image(width_img, height_img);
		oldImage.src = imageObj.src
		imageObj.src = webcam.toDataURL()

		if (!oldImage || !imageObj) {
			return;
		}

		let vals = imageCompare.compare(imageObj, oldImage, width, height);
		topLeft[0] = vals.topLeft[0] * 10;
		topLeft[1] = vals.topLeft[1] * 10;

		bottomRight[0] = vals.bottomRight[0] * 10;
		bottomRight[1] = vals.bottomRight[1] * 10;

		// Sensor 1
		let x_top_right = points_value.x + points_value.width
		let y_bottom_right = points_value.y + points_value.height
		// Sensor 2
		let x_top_right2 = points_value2.x + points_value2.width
		let y_bottom_right2 = points_value2.y + points_value2.height
		// Sensor 3
		let x_top_right3 = points_value3.x + points_value3.width
		let y_bottom_right3 = points_value3.y + points_value3.height
		// Sensor 4
		let x_top_right4 = points_value4.x + points_value4.width
		let y_bottom_right4 = points_value4.y + points_value4.height

		// Compare the points of the Konva square (sensor 1) and the movement
		if ((topLeft[0] >= points_value.x && topLeft[1] >= points_value.y && topLeft[0] <= x_top_right && topLeft[1] <= y_bottom_right) || (bottomRight[0] >= points_value.x && bottomRight[1] >= points_value.y && bottomRight[0] <= x_top_right && bottomRight[1] <= y_bottom_right)) {
			console.log("sensor 1")
			$("#select_rect1").onchange = newValue1();
			socket.emit('click', key);
			socket.on('done', function (msg) {
				movement_done = msg
				if(document.getElementById("btn1").value === "Sensor 1 activé"){
					console.log(document.getElementById("btn1").value);
					audio.play();
				}else{
					audio.pause();
				}
				
			})
		}
		// Compare the points of the Konva square (sensor 2) and the movement
		if ((topLeft[0] >= points_value2.x && topLeft[1] >= points_value2.y
			&& topLeft[0] <= x_top_right2 && topLeft[1] <= y_bottom_right2)
			||
			(bottomRight[0] >= points_value2.x && bottomRight[1] >= points_value2.y
				&& bottomRight[0] <= x_top_right2 && bottomRight[1] <= y_bottom_right2)) {
			console.log("sensor 2")
			$("#select_rect2").onchange = newValue2();
			socket.emit('click', key2);
			socket.on('done', function (msg) {
				movement_done = msg
				if(document.getElementById("btn2").value === "Sensor 2 activé"){
					audio.play();
				}else{
					audio.pause();
				}
			})
		}

		// Compare the points of the Konva square (sensor 3) and the movement
		if ((topLeft[0] >= points_value3.x && topLeft[1] >= points_value3.y
			&& topLeft[0] <= x_top_right3 && topLeft[1] <= y_bottom_right3)
			||
			(bottomRight[0] >= points_value3.x && bottomRight[1] >= points_value3.y
				&& bottomRight[0] <= x_top_right3 && bottomRight[1] <= y_bottom_right3)) {
			console.log("sensor 3")
			$("#select_rect3").onchange = newValue3();
			socket.emit('click', key3);
			socket.on('done', function (msg) {
				movement_done = msg
				if(document.getElementById("btn3").value === "Sensor 3 activé"){
					audio.play();
				}else{
					audio.pause();
				}
			})
		}

		// Compare the points of the Konva square (sensor 4) and the movement
		if ((topLeft[0] >= points_value4.x && topLeft[1] >= points_value4.y
			&& topLeft[0] <= x_top_right4 && topLeft[1] <= y_bottom_right4)
			||
			(bottomRight[0] >= points_value4.x && bottomRight[1] >= points_value4.y
				&& bottomRight[0] <= x_top_right4 && bottomRight[1] <= y_bottom_right4)) {
			console.log("sensor 4")
			$("#select_rect4").onchange = newValue4();
			socket.emit('click', key4);
			socket.on('done', function (msg) {
				movement_done = msg
				if(document.getElementById("btn4").value === "Sensor 4 activé"){
					audio.play();
				}else{
					audio.pause();
				}
			})
		}
	}

	function newValue1() {
		key = $("#select_rect1").val().toLowerCase();
	}

	function newValue2() {
		key2 = $("#select_rect2").val().toLowerCase();
	}

	function newValue3() {
		key3 = $("#select_rect3").val().toLowerCase();
	}

	function newValue4() {
		key4 = $("#select_rect4").val().toLowerCase();
	}

	/*
     * The main rendering loop.
     */
	function main() {
		try {
			if (Object.keys(points_value).length === 0 && points_value.constructor === Object) {
				// points_value of the square empty : no movement needed
			} else {
				render();
			}
		} catch (e) {
			console.log(e);
			return;
		}

		try {
			if (Object.keys(points_value2).length === 0 && points_value2.constructor === Object) {
				// points_value of the square empty : no movement needed
			} else {
				render();
			}
		} catch (e) {
			console.log(e);
			return;
		}

		try {
			if (Object.keys(points_value3).length === 0 && points_value3.constructor === Object) {
				// points_value of the square empty : no movement needed
			} else {
				render();
			}
		} catch (e) {
			console.log(e);
			return;
		}

		try {
			if (Object.keys(points_value4).length === 0 && points_value4.constructor === Object) {
				// points_value of the square empty : no movement needed
			} else {
				render();
			}
		} catch (e) {
			console.log(e);
			return;
		}

		if (rendering === true) {
			let anim = new Konva.Animation(function () {
			}, layer)
			raf(main.bind(anim.start()))
		}
	}

	initialize();
}
