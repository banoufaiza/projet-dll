let points_value = {}
let points = {}
let key;

//Part linked to the rectangles
let MIN_X = 2
let MIN_Y = 2
let MAX_WIDTH = 700;
let MIN_WIDTH = 150;
let MAX_HEIGHT = 600;
let MIN_HEIGHT = 400;

// The blue rectangle
let rect1 = new Konva.Rect({
    x: 0,
    y: stage.height() - (MAX_HEIGHT/2),
    width: 150,
    height: 300,
    stroke: 'blue',
    strokeWidth:0,
    visible: false,
    name: 'rect1',
    draggable: true
});

// Transformer of rect (the outside of the rectangle)
let tr = new Konva.Transformer({
    boundBoxFunc: function(oldBoundBox, newBoundBox) {
    let MAX_X = newBoundBox.x + newBoundBox.width
    let MAX_Y = newBoundBox.y + newBoundBox.height
        
      // Points set when the rectangle is descreased of increased
      if (newBoundBox.x < MIN_X) {
        tr.stopTransform();
        newBoundBox.x = MIN_X;
      }
      if (MAX_X > MAX_WIDTH) {
        tr.stopTransform();
        newBoundBox.x = MAX_WIDTH - newBoundBox.width;
      }
      if (newBoundBox.y < MIN_Y) {
        tr.stopTransform();
        newBoundBox.y = MIN_Y;
      }
      if (MAX_Y > MAX_HEIGHT) {
        tr.stopTransform();
        newBoundBox.y = MAX_HEIGHT - newBoundBox.height;
      }

      if (newBoundBox.width > MAX_WIDTH) {
        newBoundBox.width = MAX_WIDTH;
      }
      if (newBoundBox.width < MIN_WIDTH) {
        newBoundBox.width = MIN_WIDTH;
      }
      if (newBoundBox.height > MAX_HEIGHT) {
        newBoundBox.height = MAX_HEIGHT;
      }
      if (newBoundBox.height < MIN_HEIGHT) {
        newBoundBox.height = MIN_HEIGHT;
      }
      
      points = {
          "x": newBoundBox.x,
          "y":newBoundBox.y,
          "width":newBoundBox.width,
          "height":newBoundBox.height
      }
      return newBoundBox;
    }
  });

//Type of mouse for the rectangle
rect1.on('mouseenter', function() {
    stage.container().style.cursor = 'move';
});

rect1.on('mouseleave', function() {
    stage.container().style.cursor = 'default';
});

//rect2
let points_value2 = {}
let points2 = {}
let key2;

//Part linked to the rectangles
let MIN_X2 = 1
let MIN_Y2 = 1
let MAX_WIDTH2 = 700;
let MIN_WIDTH2 = 150;
let MAX_HEIGHT2 = 600;
let MIN_HEIGHT2 = 400;

// The blue rectangle
let rect2 = new Konva.Rect({
    x: stage.width() - 150,
    y: stage.height() - (MAX_HEIGHT2/2),
    width: 150,
    height: 300,
    stroke: 'green',
    strokeWidth:0,
    visible: false,
    name: 'rect2',
    draggable: true
});

//console.log(rect2.attrs)

// Transformer of rect (the outside of the rectangle)
let tr2 = new Konva.Transformer({
    boundBoxFunc: function(oldBoundBox, newBoundBox) {
        let MAX_X2 = newBoundBox.x + newBoundBox.width
        let MAX_Y2 = newBoundBox.y + newBoundBox.height

        // Points set when the rectangle is descreased of increased
        if (newBoundBox.x < MIN_X2) {
            tr2.stopTransform();
            newBoundBox.x = MIN_X;
        }
        if (MAX_X2 > MAX_WIDTH2) {
            tr2.stopTransform();
            newBoundBox.x = MAX_WIDTH2 - newBoundBox.width;
        }
        if (newBoundBox.y < MIN_Y2) {
            tr2.stopTransform();
            newBoundBox.y = MIN_Y2;
        }
        if (MAX_Y2 > MAX_HEIGHT2) {
            tr2.stopTransform();
            newBoundBox.y = MAX_HEIGHT2 - newBoundBox.height;
        }

        if (newBoundBox.width > MAX_WIDTH2) {
            newBoundBox.width = MAX_WIDTH2;
        }
        if (newBoundBox.width < MIN_WIDTH2) {
            newBoundBox.width = MIN_WIDTH2;
        }
        if (newBoundBox.height > MAX_HEIGHT2) {
            newBoundBox.height = MAX_HEIGHT2;
        }
        if (newBoundBox.height < MIN_HEIGHT2) {
            newBoundBox.height = MIN_HEIGHT2;
        }

        points2 = {
            "x": newBoundBox.x,
            "y":newBoundBox.y,
            "width":newBoundBox.width,
            "height":newBoundBox.height
        }
        return newBoundBox;
    }
});

//Type of mouse for the rectangle
rect2.on('mouseenter', function() {
    stage.container().style.cursor = 'move';
});

rect2.on('mouseleave', function() {
    stage.container().style.cursor = 'default';
});


//rect3
let points_value3 = {}
let points3 = {}
let key3;

//Part linked to the rectangles
let MIN_X3 = 1
let MIN_Y3 = 1
let MAX_WIDTH3 = 700;
let MIN_WIDTH3 = 150;
let MAX_HEIGHT3 = 600;
let MIN_HEIGHT3 = 400;

// The blue rectangle
let rect3 = new Konva.Rect({
    x: stage.width() - 700,
    y: stage.height() - (MAX_HEIGHT2),
    width: 150,
    height: 300,
    stroke: 'green',
    strokeWidth:0,
    visible: false,
    name: 'rect3',
    draggable: true
});

//console.log(rect3.attrs)

// Transformer of rect (the outside of the rectangle)
let tr3 = new Konva.Transformer({
    boundBoxFunc: function(oldBoundBox, newBoundBox) {
        let MAX_X3 = newBoundBox.x + newBoundBox.width
        let MAX_Y3 = newBoundBox.y + newBoundBox.height

        // Points set when the rectangle is descreased of increased
        if (newBoundBox.x < MIN_X3) {
            tr3.stopTransform();
            newBoundBox.x = MIN_X;
        }
        if (MAX_X3 > MAX_WIDTH3) {
            tr3.stopTransform();
            newBoundBox.x = MAX_WIDTH3 - newBoundBox.width;
        }
        if (newBoundBox.y < MIN_Y3) {
            tr3.stopTransform();
            newBoundBox.y = MIN_Y3;
        }
        if (MAX_Y3 > MAX_HEIGHT3) {
            tr3.stopTransform();
            newBoundBox.y = MAX_HEIGHT3 - newBoundBox.height;
        }

        if (newBoundBox.width > MAX_WIDTH3) {
            newBoundBox.width = MAX_WIDTH3;
        }
        if (newBoundBox.width < MIN_WIDTH3) {
            newBoundBox.width = MIN_WIDTH3;
        }
        if (newBoundBox.height > MAX_HEIGHT3) {
            newBoundBox.height = MAX_HEIGHT3;
        }
        if (newBoundBox.height < MIN_HEIGHT3) {
            newBoundBox.height = MIN_HEIGHT3;
        }

        points3 = {
            "x": newBoundBox.x,
            "y":newBoundBox.y,
            "width":newBoundBox.width,
            "height":newBoundBox.height
        }
        return newBoundBox;
    }
});

//Type of mouse for the rectangle
rect3.on('mouseenter', function() {
    stage.container().style.cursor = 'move';
});

rect3.on('mouseleave', function() {
    stage.container().style.cursor = 'default';
});

//rect4
let points_value4 = {}
let points4 = {}
let key4;

//Part linked to the rectangles
let MIN_X4 = 1
let MIN_Y4 = 1
let MAX_WIDTH4 = 700;
let MIN_WIDTH4 = 150;
let MAX_HEIGHT4 = 600;
let MIN_HEIGHT4 = 400;

// The blue rectangle
let rect4 = new Konva.Rect({
    x: stage.width() - 150,
    y: stage.height() - (MAX_HEIGHT),
    width: 150,
    height: 300,
    stroke: 'green',
    strokeWidth:0,
    visible: false,
    name: 'rect4',
    draggable: true
});

//console.log(rect2.attrs)

// Transformer of rect (the outside of the rectangle)
let tr4 = new Konva.Transformer({
    boundBoxFunc: function(oldBoundBox, newBoundBox) {
        let MAX_X4 = newBoundBox.x + newBoundBox.width
        let MAX_Y4 = newBoundBox.y + newBoundBox.height

        // Points set when the rectangle is descreased of increased
        if (newBoundBox.x < MIN_X4) {
            tr4.stopTransform();
            newBoundBox.x = MIN_X4;
        }
        if (MAX_X4 > MAX_WIDTH4) {
            tr4.stopTransform();
            newBoundBox.x = MAX_WIDTH4 - newBoundBox.width;
        }
        if (newBoundBox.y < MIN_Y4) {
            tr4.stopTransform();
            newBoundBox.y = MIN_Y4;
        }
        if (MAX_Y4 > MAX_HEIGHT4) {
            tr4.stopTransform();
            newBoundBox.y = MAX_HEIGHT4 - newBoundBox.height;
        }

        if (newBoundBox.width > MAX_WIDTH4) {
            newBoundBox.width = MAX_WIDTH4;
        }
        if (newBoundBox.width < MIN_WIDTH4) {
            newBoundBox.width = MIN_WIDTH4;
        }
        if (newBoundBox.height > MAX_HEIGHT4) {
            newBoundBox.height = MAX_HEIGHT4;
        }
        if (newBoundBox.height < MIN_HEIGHT4) {
            newBoundBox.height = MIN_HEIGHT4;
        }

        points4 = {
            "x": newBoundBox.x,
            "y":newBoundBox.y,
            "width":newBoundBox.width,
            "height":newBoundBox.height
        }
        return newBoundBox;
    }
});

//Type of mouse for the rectangle
rect4.on('mouseenter', function() {
    stage.container().style.cursor = 'move';
});

rect4.on('mouseleave', function() {
    stage.container().style.cursor = 'default';
});

let modeVal;
/*
$("#modes").change(function(e) {
    modeVal = e.target.options[e.target.options.selectedIndex].value
    if(modeVal === "presentation") {
        console.log("mode presentation")
        $("#btn-sensor1").prop("disabled", true)
        $("#btn-sensor2").prop("disabled", true)
    } else if(modeVal === "drawing") {
        console.log("mode dessin")
        $("#btn-sensor1").prop("disabled", false)
        $("#btn-sensor2").prop("disabled", false)
    } else if(modeVal === "navigation") {
        console.log("mode navigation")
        $("#btn-sensor1").prop("disabled", false)
        $("#btn-sensor2").prop("disabled", false)
    }
})*/

// Start to show the rectangle when the button Sensor 1 is clicked and change value of button
function changeS1() {
    if (document.getElementById("btn1").value === "Sensor 1 désactivé") {
        document.getElementById("btn1").value = "Sensor 1 activé";
        document.getElementById("btn1").style.backgroundColor= "green";
        document.getElementById("btn1").style.color= "white";
        $('#select_rect1').removeAttr('disabled');
        layer.add(rect1);
        layer.add(tr);
        tr.attachTo(rect1);
        rect1.show();
        tr.show()

        layer.draw();
    } else {
        document.getElementById("btn1").value = "Sensor 1 désactivé";
        document.getElementById("btn1").style.backgroundColor= "blue";
        document.getElementById("btn1").style.color= "white";
        $('#select_rect1').attr('disabled', true);
        rect1.hide();
        tr.hide();

        layer.draw();
        points_value = points
    }
}


// Start to show the rectangle when the button Sensor 2 is clicked and change value of button
function changeS2() {
    if (document.getElementById("btn2").value === "Sensor 2 désactivé") {
        document.getElementById("btn2").value = "Sensor 2 activé";
        document.getElementById("btn2").style.backgroundColor= "green";
        document.getElementById("btn2").style.color= "white";

        $('#select_rect2').removeAttr('disabled');
        layer.add(rect2);
        layer.add(tr2);
        tr2.attachTo(rect2);
        rect2.show();
        tr2.show()

        layer.draw();
    } else {
        document.getElementById("btn2").value = "Sensor 2 désactivé";
        document.getElementById("btn2").style.backgroundColor= "blue";
        document.getElementById("btn2").style.color= "white";

        $('#select_rect2').attr('disabled', true);
        rect2.hide();
        tr2.hide();

        layer.draw();
        points_value2 = points2
    }
}

// Start to show the rectangle when the button Sensor 3 is clicked and change value of button
function changeS3() {
    if (document.getElementById("btn3").value === "Sensor 3 désactivé") {
        document.getElementById("btn3").value = "Sensor 3 activé";
        document.getElementById("btn3").style.backgroundColor= "green";
        document.getElementById("btn3").style.color= "white";

        $('#select_rect3').removeAttr('disabled');
        layer.add(rect3);
        layer.add(tr3);
        tr3.attachTo(rect3);
        rect3.show();
        tr3.show()

        layer.draw();
    } else {
        document.getElementById("btn3").value = "Sensor 3 désactivé";
        document.getElementById("btn3").style.backgroundColor= "blue";
        document.getElementById("btn3").style.color= "white";

        $('#select_rect3').attr('disabled', true);
        rect3.hide();
        tr3.hide();

        layer.draw();
        points_value3 = points3
    }
}

// Start to show the rectangle when the button Sensor 4 is clicked and change value of button
function changeS4() {
    if (document.getElementById("btn4").value === "Sensor 4 désactivé") {
        document.getElementById("btn4").value = "Sensor 4 activé";
        document.getElementById("btn4").style.backgroundColor= "green";
        document.getElementById("btn4").style.color= "white";

        $('#select_rect4').removeAttr('disabled');
        layer.add(rect4);
        layer.add(tr4);
        tr4.attachTo(rect4);
        rect4.show();
        tr4.show()

        layer.draw();
    } else {
        document.getElementById("btn4").value = "Sensor 4 désactivé";
        document.getElementById("btn4").style.backgroundColor= "blue";
        document.getElementById("btn4").style.color= "white";

        $('#select_rect4').attr('disabled', true);
        rect4.hide();
        tr4.hide();

        layer.draw();
        points_value4 = points4
    }
}


// Both
if ((document.getElementById("btn1").value == "Sensor 1 activé") && (document.getElementById("btn2").value = "Sensor 2 activé") && (document.getElementById("btn3").value = "Sensor 3 activé") && (document.getElementById("btn4").value = "Sensor 4 activé") ){
    $('#select_rect1').removeAttr('disabled');
    $('#select_rect2').removeAttr('disabled');
    $('#select_rect3').removeAttr('disabled');
    $('#select_rect4').removeAttr('disabled');
    layer.add(rect1);
    layer.add(tr);
    tr.attachTo(rect1);
    rect1.show();
    tr.show()

    layer.add(rect2);
    layer.add(tr2);
    tr2.attachTo(rect2);
    rect2.show();
    tr2.show()

    layer.add(rect3);
    layer.add(tr3);
    tr3.attachTo(rect3);
    rect3.show();
    tr3.show()

    layer.add(rect4);
    layer.add(tr4);
    tr4.attachTo(rect4);
    rect4.show();
    tr4.show()

    layer.draw();
}


$('#threshold').click(function () {
    if ($(this).is(':checked')) {
        key = $("#select_rect1").val().toLowerCase();
        // if points_value is empty (not dragged or transformed)
        if (Object.keys(points).length === 0 && points.constructor === Object) {
            points_value = {
                "x": rect1.x(),// + sensor1DivWidth,
                "y": rect1.y() + titleHeight,
                "width": rect1.width(),
                "height": rect1.height()
            }
            console.log(points_value)
        } else {
            points_value = points
        }
    } else {
        points_value = {}
    }
});

$('#threshold').click(function () {
    if (($(this).is(':checked'))) {
        $("#select_rect2").onchange = newValue();
        key2 = $("#select_rect2").val().toLowerCase();
        // if points_value is empty (not dragged or transformed)
        if (Object.keys(points2).length === 0 && points2.constructor === Object) {
            points_value2 = {
                "x": rect2.x(),// + sensor1DivWidth,
                "y": rect2.y() + titleHeight,
                "width": rect2.width(),
                "height": rect2.height()
            }

        } else {
            points_value2 = points2
        }
    } else {
        points_value2 = {}
    }
});

$('#threshold').click(function () {
    if (($(this).is(':checked'))) {
        $("#select_rect3").onchange = newValue();
        key3 = $("#select_rect3").val().toLowerCase();
        // if points_value is empty (not dragged or transformed)
        if (Object.keys(points3).length === 0 && points3.constructor === Object) {
            points_value3 = {
                "x": rect3.x(),// + sensor1DivWidth,
                "y": rect3.y() + titleHeight,
                "width": rect3.width(),
                "height": rect3.height()
            }

        } else {
            points_value3 = points3
        }
    } else {
        points_value3 = {}
    }
});

$('#threshold').click(function () {
    if (($(this).is(':checked'))) {
        $("#select_rect4").onchange = newValue();
        key4 = $("#select_rect4").val().toLowerCase();
        // if points_value is empty (not dragged or transformed)
        if (Object.keys(points4).length === 0 && points4.constructor === Object) {
            points_value4 = {
                "x": rect4.x(),// + sensor1DivWidth,
                "y": rect4.y() + titleHeight,
                "width": rect4.width(),
                "height": rect4.height()
            }

        } else {
            points_value4 = points4
        }
    } else {
        points_value4 = {}
    }
});

function newValue() {
    key2 = $("#select_rect2").val().toLowerCase();
}

function newValue() {
    key3 = $("#select_rect3").val().toLowerCase();
}

function newValue() {
    key4 = $("#select_rect4").val().toLowerCase();
}