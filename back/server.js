const express           = require('express'),
      cors              = require('cors'),
      app               = express(),
      robot             = require("robotjs"),
      { createServer }  = require("http"),
      { Server }        = require("socket.io");

// ------------------------
// CONFIG EXPRESS
// ------------------------
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// ------------------------
// CONFIG SOCKET.IO
// ------------------------
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost",
    methods: ["GET", "POST"],
  },
});

// ------------------------
// Connection socket.io to receive the key 
// Use robojs to press the key
// ------------------------
io.on('connection', function (socket) {
  console.log("socket");
  socket.on('click', function(key) {
    //If the type of key is a mouse
    console.log("this is "+key);
    if (key.includes("mouse_")) {
      let mouse = robot.getMousePos();
      if (key === 'mouse_up'){
        robot.moveMouseSmooth(mouse.x, mouse.y - 50);
      } else if (key === 'mouse_down'){
        robot.moveMouseSmooth(mouse.x, mouse.y + 50);
      }else if (key === 'mouse_left'){
        robot.moveMouseSmooth(mouse.x - 50, mouse.y);
      }else if (key === 'mouse_right'){
        robot.moveMouseSmooth(mouse.x + 50, mouse.y);
      }
    //If the type of key is key press
    } else {
      robot.keyTap(key);
    }
    socket.emit('done', true);
        
  });
  socket.on('move',function(key){
    console.log(key);
    let mouse = robot.getMousePos();
    if(key === "go_up"){
      
      robot.moveMouseSmooth(mouse.x, mouse.y - 50);
    } else if (key === 'thumbs_down'){
      robot.moveMouseSmooth(mouse.x, mouse.y + 50);
    }else if (key === 'thumbs_left'){
      robot.moveMouseSmooth(mouse.x + 50, mouse.y);
    }else if (key === 'thumbs_right'){
      robot.moveMouseSmooth(mouse.x - 50, mouse.y);
    }
   /* else if (key === 'thumbs_Curl'){
      app.get('/', (res) => {
        
res.redirect('http://www.google.com')
      })
    }*/
  });
});


// ------------------------
// START LISTENING SERVER
// ------------------------
httpServer.listen(3000, function() {
    console.info('HTTP server started on port 3000');
});