# Projet DLL 6 : Pilotage gestuel d'un logiciel
Membres du groupe:
- Lola AIGUEPERSE
- Faïza BOUMALLOUK
- Lydie JEANTY
- Oumaima CHAMMAKHI

## Description
Le projet consiste à réaliser une application web qui permet de piloter un logiciel gestuellement.

Nous avons repris un projet existant créé par d’anciens étudiants accessible à cette adresse : https://github.com/MarionLnd/SviacamReprise .

Notre objectif était de continuer le développement de ce projet pour ajouter/modifier des fonctionnalités existantes telles que :
* Supprimer la détection de mouvement (zone rouge)
* Autoriser le redimensionnement des zones
* Autoriser un nombre de zones plus grand (actuellement la limite est de deux zones)
* Créer l'interface utilisateur pour les gestes des doigts (actuellement l’utilisateur n’est pas guidé)

## Documentation
Un [wiki](https://gitlab.com/banoufaiza/projet-dll/-/wikis/home) est à disposition. Vous y trouverez des explications pour l'installation et l'utilisation de l'application.

## Technologies et librairies utilisées
* Côté serveur:
  * [Node.js](https://nodejs.org)
  * [Express.js](https://expressjs.com/)
  * [Socket.io](https://socket.io/)
  * [RobotJS](https://robotjs.io)
* Côté client:
  * Javascript (+ JQuery)
  * [Bootstrap](https://getbootstrap.com/)
  * [Konva](https://konvajs.org/)
  * [Tensorflow](https://www.tensorflow.org): [Fingerpose](https://github.com/andypotato/fingerpose), [model handpose](https://github.com/tensorflow/tfjs-models/tree/master/handpose)

## Docker
Architecture faite avec [Docker](https://www.docker.com/) avec la présence de 2 containers:
1. Front sur le port 80
1. Serveur Node.js sur le port 3000

## Installation du projet
Pré-requis: avoir Docker installé et lancé

1. Récupérer le projet Git: ``` git clone https://gitlab.com/banoufaiza/projet-dll ```
1. Se déplacer dans le projet: ``` cd projet-dll ```
1. Création des images Docker: ``` docker-compose -f docker-compose.yml build ```
1. Lancer les containers Docker: ``` docker-compose -f docker-compose.yml up -d ```
1. Eteindre les containers Docker: ``` docker-compose -f docker-compose.yml down ```
